﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatHandler : MonoBehaviour
{
    public DatabaseAPI database;

    public InputField name_field;
    public InputField message_field;

    public GameObject messageprefab;
    public Transform messageContainer;

    private void Start()
    {
        database.Listen(instantiatemsg, Debug.Log);
    }
    public void sendmessage()
    {
        database.PostMessage(new Message(name_field.text, message_field.text), () =>
         {
             Debug.Log("Message was sent");
         }, exception =>
         {
             Debug.Log(exception);
         });
    }

    private void instantiatemsg(Message message)
    {
        GameObject new_message = Instantiate(messageprefab, transform.position, Quaternion.identity);
        new_message.transform.SetParent(messageContainer, false);
        new_message.GetComponent<Text>().text = string.Format("{0} : {1}", message.sender, message.text);
    }
}   