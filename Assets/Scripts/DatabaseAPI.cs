﻿using UnityEngine;
using Firebase;
using System;
using Firebase.Database;

public class DatabaseAPI : MonoBehaviour
{
    private DatabaseReference reference;
    private void Awake()
    {
        FirebaseApp.DefaultInstance.Options.DatabaseUrl = new System.Uri("https://trail-9c4ac-default-rtdb.firebaseio.com/");
        reference = FirebaseDatabase.DefaultInstance.RootReference;
    }
    public void PostMessage(Message message,Action callback,Action<AggregateException> fallback)
    {
        var messageJSON = StringSerializationAPI.Serialize(typeof(Message), message);
        
        reference.Child("Messages").Push().SetRawJsonValueAsync(messageJSON).ContinueWith(task =>
        {
            if (task.IsCanceled || task.IsFaulted)
            {
                fallback(task.Exception);
            }

            else callback();
        });
    }

    public void Listen(Action<Message> callback, Action<AggregateException> fallback)
    {
        void CurrentListener(object o,ChildChangedEventArgs args)
        {
            if (args.DatabaseError != null)
            {
                fallback(new AggregateException(new Exception(args.DatabaseError.Message)));
            }
            else callback(StringSerializationAPI.Deserialize(typeof(Message), serializedState: args.Snapshot.GetRawJsonValue())as Message);
            
        }

        reference.Child("Messages").ChildAdded += CurrentListener;
    }
}
